provider "aws" {
    region = var.region 
}

variable "region" {}
variable "avail_zone" {
    description = "AZs to use"
    default = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c"]
    type = list
}
variable "my_ip" {}
variable "cidr_blocks" {
    description = "vpc and subnets names and cidr blocks"
    type = list(object({
        cidr_block = string
        name = string
    }))
}
variable "inbound_rules_cp_nodes" {}
variable "inbound_rules_worker_nodes" {}
variable "outbound_rules_cp_nodes" {}
variable "outbound_rules_worker_nodes" {}
variable "instance_type_cp_node" {}
variable "instance_type_worker_node" {}
variable "public_key_location" {}

//Creating VPC
resource "aws_vpc" "k8s-vpc" {
    cidr_block = var.cidr_blocks[0].cidr_block
    enable_dns_hostnames = true
    tags = {
        Name: var.cidr_blocks[0].name
    }  
}

//Creating subnets
resource "aws_subnet" "k8s-subnet" {
    count = "${length(var.avail_zone)}"
    vpc_id = aws_vpc.k8s-vpc.id
    cidr_block = "${var.cidr_blocks[count.index+1].cidr_block}"
    availability_zone = "${var.avail_zone[count.index]}"
    tags = {
        Name: var.cidr_blocks[count.index+1].name
    }
}

//Creating IGW
resource "aws_internet_gateway" "k8s-igw" {
    vpc_id = aws_vpc.k8s-vpc.id
    tags = {
        Name: "${var.cidr_blocks[0].name}-igw"
    } 
}

//Creating a RT
resource "aws_route_table" "k8s-routing-table" {
    vpc_id = aws_vpc.k8s-vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.k8s-igw.id

    }
    tags = {
        Name: "${var.cidr_blocks[0].name}-rtb"
    }
}

//Associating the subnets with the RT 
resource "aws_route_table_association" "k8s-routing-table-subnet-association" {
    count = "${length(var.avail_zone)}"
    subnet_id = "${element(aws_subnet.k8s-subnet.*.id, count.index)}"
    route_table_id = "${aws_route_table.k8s-routing-table.id}"
}

//Creating a SG for the control plane nodes
resource "aws_security_group" "k8s-cp-sg"{
    name = "k8s-cp-sg"
    vpc_id =aws_vpc.k8s-vpc.id

    dynamic "ingress" {
        for_each = var.inbound_rules_cp_nodes
        content {
            description = ingress.value.description
            from_port   = ingress.value.from_port
            to_port     = ingress.value.to_port
            protocol    = ingress.value.protocol
            cidr_blocks = [ingress.value.cidr_block]
        }
    }

    dynamic "egress" {
        for_each = var.outbound_rules_cp_nodes
        content {
            description = egress.value.description
            from_port   = egress.value.from_port
            to_port     = egress.value.to_port
            protocol    = egress.value.protocol
            cidr_blocks = [egress.value.cidr_block]
        }
    }

    tags = {
        Name: "k8s-cp-sg"
    }
}

//Creating a SG for the worker nodes
resource "aws_security_group" "k8s-worker-sg"{
    name = "k8s-worker-sg"
    vpc_id =aws_vpc.k8s-vpc.id

    dynamic "ingress" {
        for_each = var.inbound_rules_worker_nodes
        content {
            description = ingress.value.description
            from_port   = ingress.value.from_port
            to_port     = ingress.value.to_port
            protocol    = ingress.value.protocol
            cidr_blocks = [ingress.value.cidr_block]
        }
    }

    dynamic "egress" {
        for_each = var.outbound_rules_worker_nodes
        content {
            description = egress.value.description
            from_port   = egress.value.from_port
            to_port     = egress.value.to_port
            protocol    = egress.value.protocol
            cidr_blocks = [egress.value.cidr_block]
        }
    }

    tags = {
        Name: "k8s-worker-sg"
    }
}

//Retrieving the most recent Ubuntu 20.04 AMI ID
data "aws_ami" "ubuntu-image" {
    most_recent = true
    owners = ["099720109477"]
    filter {
        name = "name" 
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]

    }
}

//Output the most recent Ubuntu 20.04 AMI ID 
output "aws_ami_id" {
    value = data.aws_ami.ubuntu-image.id
}

//Creating an SSH key
resource "aws_key_pair" "ssh-key" {
    key_name = "k8s-key"
    public_key = file(var.public_key_location)
}

//Creating K8s control plane nodes (one per availability zone)
resource "aws_instance" "k8s-cp-node" {

    count = "${length(var.avail_zone)}"
    availability_zone = "${var.avail_zone[count.index]}"
    ami = data.aws_ami.ubuntu-image.id
    instance_type = var.instance_type_cp_node
    subnet_id = "${element(aws_subnet.k8s-subnet.*.id, count.index)}"
    vpc_security_group_ids = [aws_security_group.k8s-cp-sg.id]
    associate_public_ip_address = true
    key_name = aws_key_pair.ssh-key.key_name
    tags = {
        Name: "k8s-cp-node-${count.index + 1}",
        Role: "k8s-cp-node"
    }
}

//Creating K8s worker nodes (one per availability zone)
resource "aws_instance" "k8s-worker-node" {

    count = "${length(var.avail_zone)}"
    availability_zone = "${var.avail_zone[count.index]}"
    ami = data.aws_ami.ubuntu-image.id
    instance_type = var.instance_type_worker_node
    subnet_id = "${element(aws_subnet.k8s-subnet.*.id, count.index)}"
    vpc_security_group_ids = [aws_security_group.k8s-worker-sg.id]
    associate_public_ip_address = true
    key_name = aws_key_pair.ssh-key.key_name
    tags = {
        Name: "k8s-worker-node-${count.index + 1}",
        Role: "k8s-worker-node"
    }
}