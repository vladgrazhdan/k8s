region = "ap-southeast-2"
avail_zone = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c"]
cidr_blocks = [
    {cidr_block = "10.0.0.0/16", name = "k8s-VPC"}, 
    {cidr_block = "10.0.16.0/20", name = "k8s-subnet1"},
    {cidr_block = "10.0.32.0/20", name = "k8s-subnet2"},
    {cidr_block = "10.0.48.0/20", name = "k8s-subnet3"}]
my_ip = "120.22.99.76/32"

inbound_rules_cp_nodes = [
    {from_port = 22, to_port = 22, protocol = "tcp", cidr_block = "120.22.99.76/32", description = "SSH"},
    {from_port = 6443, to_port = 6443, protocol = "tcp", cidr_block = "0.0.0.0/0", description = "Kubernetes API server"},
    {from_port = 2379, to_port = 2380, protocol = "tcp", cidr_block = "10.0.0.0/16", description = "etcd server client API"},
    {from_port = 10250, to_port = 10252, protocol = "tcp", cidr_block = "10.0.0.0/16", description = "kubelet API, kube-scheduler, kube-controller-manager"}
]

inbound_rules_worker_nodes = [
    {from_port = 22, to_port = 22, protocol = "tcp", cidr_block = "120.22.99.76/32", description = "SSH"},
    {from_port = 10250, to_port = 10250, protocol = "tcp", cidr_block = "10.0.0.0/16", description = "kubelet API"},
    {from_port = 30000, to_port = 32767, protocol = "tcp", cidr_block = "0.0.0.0/0", description = "NodePort Services"}
]

outbound_rules_cp_nodes = [
    {from_port = 0, to_port = 0, protocol = "-1", cidr_block = "0.0.0.0/0", description = "Allow All"}
]

outbound_rules_worker_nodes = [
    {from_port = 0, to_port = 0, protocol = "-1", cidr_block = "0.0.0.0/0", description = "Allow All"}
]

instance_type_cp_node = "t3a.small"
instance_type_worker_node = "t3a.small"
public_key_location = "~/.ssh/k8s-key.pub"