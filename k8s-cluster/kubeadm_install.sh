#!/bin/bash

# installing packages needed to use the K8s apt repo
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl

# downloading the Google Cloud public signing key
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

# adding the Kubernetes apt repo
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# installing kubelet, kubeadm, and kubectl of the same version (apt-cache madison kubeadm to check). Pin their version
sudo apt-get update
sudo apt-get install -y kubelet=1.24.2-00 kubeadm=1.24.2-00 kubectl=1.24.2-00
sudo apt-mark hold kubelet kubeadm kubectl