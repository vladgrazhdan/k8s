#!/bin/bash

# loading required modules at boot
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

# adding modules to the kernel
sudo modprobe overlay
sudo modprobe br_netfilter

# sysctl params required by setup, params persist across reboots
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

# applying sysctl params without reboot
sudo sysctl --system

# installing containerd cri
sudo apt-get update
sudo apt-get -y install containerd

# installing cni plugins (not installed by apt-get)
sudo mkdir -p /opt/cni/bin
wget https://github.com/containernetworking/plugins/releases/download/v1.1.1/cni-plugins-linux-amd64-v1.1.1.tgz
sudo tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.1.1.tgz

# creating containerd default config 
sudo mkdir /etc/containerd
containerd config default | sudo tee -a /etc/containerd/config.toml

# restarting containerd & checking status
sudo systemctl restart containerd
service containerd status

