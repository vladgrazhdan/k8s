# Kubernetes manifests and Terraform templates

A collection of Kubernetes manifests. 

Terraform + Ansible to build EC2-based cluster. 

## Intro

EC2 based K8s cluster with Terraform + Ansible.

## Overview

*   [x] **Can be modified and extended**
*   [x] **Tested**

### Requirements

* Terraform v1.2.5
* Ansible v2.12.10
